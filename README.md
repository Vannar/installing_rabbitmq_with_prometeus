This is the installation guide for RabbitMQ with Prometheus as the monitoring tool

# Installing RabbitMQ on Ubuntu server.

Run the following commnads

Step 1: Update the System.
-> sudo apt-get update

Step 2: Add repo & import the key.
-> echo 'deb http://www.rabbitmq.com/debian/ testing main' | sudo tee /etc/apt/sources.list.d/rabbitmq.list
wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | sudo apt-key add -

Step 3: Update the packages.
-> sudo apt-get update

Step 4: Install RabbitMQ server.
-> sudo apt-get install rabbitmq-server -y

Step 5: Start & Enable the RabbitMQ server
-> sudo systemctl start rabbitmq-server
-> sudo systemctl enable rabbitmq-server

Step 6: Check the RabbitMQ server status.
-> sudo systemctl status rabbitmq-server

Step 7: Create User in RabbitMQ server.
-> sudo rabbitmqctl add_user admin password
-> sudo rabbitmqctl set_user_tags admin administrator
-> sudo rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"

Step 8: Enable the RabbitMQ Web Management Console.
-> sudo rabbitmq-plugins enable rabbitmq_management

Here the installation is complete for the RabbitMQ
you can expose the rabbitMq webpage at port number 15672
you can refer to the "rabbitmq_webpage.png"
               or
curl localhost:15672

Note* Run this command to integrate the RabbitMQ with Prometeus.
-> sudo rabbitmq-plugins enable rabbitmq_prometheus

# Installling Prometheus on Ubuntu server\

Step 1: Create Prometheus system group
-> sudo groupadd --system prometheus
-> sudo useradd -s /sbin/nologin --system -g prometheus prometheus

Step 2: Create data & configs directories for Prometheus
-> sudo mkdir /var/lib/prometheus
-> for i in rules rules.d files_sd; do sudo mkdir -p /etc/prometheus/${i}; done

Step 3: Download Prometheus on Ubuntu
-> sudo apt update
-> sudo apt -y install wget curl vim
-> sudo mkdir -p /tmp/prometheus && cd /tmp/prometheus
-> sudo curl -s https://api.github.com/repos/prometheus/prometheus/releases/latest | grep browser_download_url | grep linux-amd64 | cut -d '"' -f 4 | wget -qi -
-> sudo tar xvf prometheus*.tar.gz
-> sudo cd prometheus*/
-> sudo sudo mv prometheus promtool /usr/local/bin/
now check the installed version :
-> prometheus --version
It will display the information regarding Prometheus if the installation is correct.
-> sudo mv prometheus.yml /etc/prometheus/prometheus.yml
-> sudo mv consoles/ console_libraries/ /etc/prometheus/
-> cd $HOME

Step 4: Configure Prometheus on Ubuntu
-> sudo vim /etc/prometheus/prometheus.yml
An editor will open in the terminal; here, add the code from file "prometheus.yml."

Create a Prometheus systemd Service unit file
To be able to manage Prometheus service with systemd, you need to explicitly define this unit file.

-> sudo nano /etc/systemd/system/prometheus.service
Again an editor will open and add the following code from file "prometheus.service" and save it!!!

Change directory permissions.
-> for i in rules rules.d files_sd; do sudo chown -R prometheus:prometheus /etc/prometheus/${i}; done
-> for i in rules rules.d files_sd; do sudo chmod -R 775 /etc/prometheus/${i}; done
-> sudo chown -R prometheus:prometheus /var/lib/prometheus/

Reload systemd daemon and start the service:
-> sudo systemctl daemon-reload
-> sudo systemctl start prometheus
-> sudo systemctl enable prometheus

Check status using systemctl status prometheus command:
-> sudo systemctl status prometheus

Prometheus is up and running we can expose the prometheus at port 9090

you can refer to the "prometheus.png" for the output

                   or 
            
-> curl localhost:9090


